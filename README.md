# Audiobook It #

This service will create an audio book from any of the inputs below in any of the languages listed below: 
  
  * an image of text
  * a spoken word audio file
  * a randomly generated piece of advice

[Supported input lanauges](https://bitbucket.org/snippets/sastrand/LeaAg8/audiobook-it-supported-input-languages)
[Supported output languages](https://bitbucket.org/snippets/sastrand/6exLpK/audiobook-it-supported-output-languages)

-----

## Running Locally ##

Set the following environment variables `GOOGLE_APPLICATION_CREDENTIALS` with the path to your GCP credentials file, and `CLOUD_STORAGE_BUCKET` with the name of a GCP storage bucket to which the service can upload input files.

Then run `python3 main.py`.

-----

This application is a final project for the CS510c-Internet, Web, and Cloud Systems course at Portland State University, Fall 2018.

### Created by ###

* Sascha Strand
* Allison Wong

APIs used:

| API                                                               | Implemented By |
|-------------------------------------------------------------------|----------------|
| [Google OCR](https://cloud.google.com/vision/docs/ocr)            | Alli           |
| [Google Translate](https://cloud.google.com/translate/)           | Sascha         |
| [Google Speech-to-Text](https://cloud.google.com/speech-to-text/) | Alli           |
| [Google Text-to-Speech](https://cloud.google.com/speech-to-text/) | Sascha         |
| [Advice Slip API](https://api.adviceslip.com/)                    | Sascha         |


