import requests

class Advice():
    def get(self):
        """
        Issues a GET request to the advice slip API and returns advice
        :rtype: string
        """
        r = requests.get('https://api.adviceslip.com/advice')
        return r.json()["slip"]["advice"]

if __name__ == "__main__":
    print("--< getting advice >--")
    a = Advice()
    print(a.get())