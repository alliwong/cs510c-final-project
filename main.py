import logging
import os
from flask import Flask, redirect, render_template, request
from google.cloud import storage
from translate import Translate
from speech_to_text import Speech_To_Text
from text_to_speech import Text_To_Speech
from advice_slip import Advice
from vision_ocr import Ocr
from uuid import uuid1

CLOUD_STORAGE_BUCKET = os.environ.get('CLOUD_STORAGE_BUCKET')

app = Flask(__name__, static_url_path='/static')


@app.route('/')
def homepage():
    t = Translate()
    languages = {'input': t.get_supported_input_langs(),
                 'output': t.get_supported_output_langs()}
    return render_template('homepage.html', languages=languages)

@app.route('/gen_audiobook', methods=['GET', 'POST'])
def gen_audiobook():
    input_lang = request.form['input_lang']
    output_lang = request.form['output_lang']
    translation_client = Translate()

    if request.form.get('advice'):
        adv = Advice()
        text_result = adv.get()
    else:
        try:
            userfile = request.files['file']
        except:
            return redirect('/') 
        # Create a Cloud Storage client.
        storage_client = storage.Client()
        # Get the bucket that the file will be uploaded to.
        bucket = storage_client.get_bucket(CLOUD_STORAGE_BUCKET)
        # Create a new blob and upload the file's content.
        blob = bucket.blob(userfile.filename)
        blob.upload_from_string(
                userfile.read(), content_type=userfile.content_type)
        # Make the blob publicly viewable.
        blob.make_public()
        # file source
        source_uri = 'gs://{}/{}'.format(CLOUD_STORAGE_BUCKET, blob.name)
        # getting the file type
        file_format = blob.name[-3:].lower()
        # checking file type
        if file_format == "wav":
            client = Speech_To_Text()
            text_result = client.transcribe(source_uri, input_lang)
        elif file_format == "jpg" or file_format == "png":
            client = Ocr()
            text_result = client.get_text(source_uri)
        else:
            return render_template('error.html', 
                msg="You must select a file of type '.wav', '.jpg', or '.png'")

    # translate text
    translation = translation_client.post(text_result, output_lang)
    # convert to audio
    tts = Text_To_Speech()
    filename = str(uuid1()) + ".mp3"
    with open('static/{}'.format(filename), "wb") as out:
            out.write(tts.get(translation, output_lang))
    return render_template('audiobook.html', filename=filename)


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
