from google.cloud import speech
from google.cloud.speech import types

"""
Class that handles transcribing audiofiles to text.
Currently only handles audiofiles of WAV format but performs the
transcription asynchronously to handle audiofiles with length
of up to 180 minutes.
"""


class Speech_To_Text():
    def __init__(self):
        self.client = speech.SpeechClient()
        self.lang_codes = {"Korean": "ko-KR",
                           "Swedish": "sv-SE",
                           "English-(US)": "en-US",
                           "French-(Candada)": "fr-CA",
                           "English-(GB)": "en-GB",
                           "French-(France)": "fr-FR",
                           "German": "de-DE",
                           "Dutch": "nl-NL",
                           "Italian": "it-IT",
                           "Spanish": "es-ES",
                           "Turkish": "tr-TR",
                           "English-(Australian)": "en-AU",
                           "Portugese-(Brazil)": "pt-BR",
                           "Japanese": "ja-JP"}

    def transcribe(self, audio_uri, audio_language):
        """
        :type audio_uri(string): location of where the audiofile is stored
        :type audio_language(string): language of audiofile
        :rtype (string) the transcribed audiofile
        """
        audio = types.RecognitionAudio(uri=audio_uri)
        config = types.RecognitionConfig(
            language_code=self.lang_codes[audio_language])
        operation = self.client.long_running_recognize(config, audio)
        response = operation.result(timeout=90)
        transcript = ""
        for result in response.results:
            transcript += result.alternatives[0].transcript
        return transcript


if __name__ == "__main__":
    s = Speech_To_Text()
    print(s.transcribe('gs://cs510c-allison-wong/autumngarden.wav','en'))
