from google.cloud import texttospeech


class Text_To_Speech():
    def __init__(self):
        self.client = texttospeech.TextToSpeechClient()
        # Map of languages supported by API and Audiobook It front end
        self.lang_codes = {"Korean": "ko-KR",
                           "Swedish": "sv-SE",
                           "English-(US)": "en-US",
                           "French-(Candada)": "fr-CA",
                           "English-(GB)": "en-GB",
                           "French-(France)": "fr-FR",
                           "German": "de-DE",
                           "Dutch": "nl-NL",
                           "Italian": "it-IT",
                           "Spanish": "es-ES",
                           "Turkish": "tr-TR",
                           "English-(Australian)": "en-AU",
                           "Portugese-(Brazil)": "pt-BR",
                           "Japanese": "ja-JP"}

    def create_synthesis_input(self, text):
        """Creates an object containing the text to be synthesized."""
        return texttospeech.types.SynthesisInput(text=text)

    def create_voice(self, language):
        """Creates an object to set the syntesis language."""
        return texttospeech.types.VoiceSelectionParams(
            language_code=self.lang_codes[language])

    def create_audio_config(self):
        """Creates an object to set the output file encoding."""
        return texttospeech.types.AudioConfig(
            audio_encoding=texttospeech.enums.AudioEncoding.MP3)

    def get(self, text, language):
        """
        Creates a GET request with the text to be synthesized, the language
        in which to syntesize it, and the output file encoding.
        """
        return self.client.synthesize_speech(
            self.create_synthesis_input(text),
            self.create_voice(language),
            self.create_audio_config()).audio_content

    def voice_lang_codes(self):
        """Creates a set of all the language codes supported by the API."""
        langs = set()
        voices = self.client.list_voices()
        for voice in voices.voices:
            if "Standard" in voice.name:
                for language_code in voice.language_codes:
                    langs.add((language_code, voice.name))
        return langs


if __name__ == "__main__":
    tts = Text_To_Speech()
    with open("output.mp3", "wb") as out:
        out.write(tts.get("Hola mundo", "Spanish"))
