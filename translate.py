from google.cloud import translate

class Translate():
    def __init__(self):
        self.client = translate.Client()
        # Dictionary of language names and codes supported at runtime
        self.target_lang_codes = {
            "English-(Australian)": "en", 
            "English-(GB)": "en", 
            "English-(US)": "en", 
            "Dutch": "nl",
            "French-(Canada)": "fr", 
            "German": "de", 
            "Italian": "it", 
            "Japanese": "ja", 
            "Korean": "ko", 
            "Portugese-(Brazil)": "pt", 
            "Spanish": "es", 
            "Swedish": "sv", 
            "Turkish": "tr",
        }

    def post(self, text, target_lang):
        """
        Creates a POST request to the translate API containing the text
        to be translated.
        """
        try:
            self.target_lang_codes[target_lang]
        except KeyError:
            return "Error: this target language is not available."
        result = self.client.translate(text, 
            target_language=self.target_lang_codes[target_lang])
        return result['translatedText']

    def get_supported_langs(self):
        """Prints a list of all language names supported by the API."""
        results = self.client.get_languages()
        for language in results:
            print(u'  * {name}    '.format(**language))

    def get_supported_input_langs(self):
        """Returns a list of all input language names supported by the API."""
        results = self.client.get_languages()
        input_languages = []
        for language in results:
            input_languages.append("{}".format(language['name']))
        return input_languages
        
    def get_supported_output_langs(self):
        """
        Returns a list of all languages that can be translated into and
        spoken with the text-to-speech API.
        """
        return ["English-(Australian)", "English-(GB)", "English-(US)", "Dutch",
                "French-(Canada)", "German", "Italian", "Japanese", "Korean", 
                "Portugese-(Brazil)", "Spanish", "Swedish", "Turkish"]

    def create_target_lang_dict(self):
        """
        Creates a dictionary of language names to lang codes 
        supported by the API.
        """
        codes = self.client.get_languages()
        codes = {x["name"]:x["language"] for x in codes}
        return codes

if __name__ == "__main__":
    t = Translate()
    print(t.target_lang_codes)
    #print(t.get_supported_input_langs())
    #print(t.get_supported_output_langs())
