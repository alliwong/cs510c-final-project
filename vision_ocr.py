from google.cloud import vision
from google.cloud.vision import types


class Ocr():
    def __init__ (self):
        self.client = vision.ImageAnnotatorClient()

    def get_text(self, image_uri):
        """
        :type image_uri: string
        :rtype: string 
        Takes in the location where the image is stored, and returns the text.
        """
        image = vision.types.Image(source=vision.types.ImageSource(gcs_image_uri=image_uri))
        resp = self.client.text_detection(image=image)
        return ' '.join([d.description for d in [resp.text_annotations[0]]])

    def get_text_language(self, image_uri):
        """
        :type image_uri: string
        :rtype: (string, string) 
        Takes in the location where the image is stored, and returns both the text and the detected language as a tuple.
        """
        image = vision.types.Image(source=vision.types.ImageSource(gcs_image_uri=image_uri))
        resp = self.client.text_detection(image=image)
        vision_text = ' '.join([d.description for d in [resp.text_annotations[0]]])
        return (vision_text, resp.text_annotations[0].locale)


